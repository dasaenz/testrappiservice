package com.test.backend.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StringUtilsTest {

	StringUtils utils = new StringUtils();
	
	@Test
	public void obtenerLinea(){
		String salto = "\n";
		String valor= "2"; 
		valor += salto+"4 5";
		valor += salto+"UPDATE 2 2 2 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"UPDATE 1 1 1 23";
		valor += salto+"QUERY 2 2 2 4 4 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"2 4";
		valor += salto+"UPDATE 2 2 2 1";
		valor += salto+"QUERY 1 1 1 1 1 1";
		valor += salto+"QUERY 1 1 1 2 2 2";
		valor += salto+"QUERY 2 2 2 2 2 2";
		String result = utils.obtenerLinea(valor,0);
		assertNotNull(result);
		assertTrue(!result.equals(""));
	}
	
	@Test
	public void obtenerValorPorEspacio(){
		String salto = "\n";
		String valor= "2"; 
		valor += salto+"4 5";
		valor += salto+"UPDATE 2 2 2 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"UPDATE 1 1 1 23";
		valor += salto+"QUERY 2 2 2 4 4 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"2 4";
		valor += salto+"UPDATE 2 2 2 1";
		valor += salto+"QUERY 1 1 1 1 1 1";
		valor += salto+"QUERY 1 1 1 2 2 2";
		valor += salto+"QUERY 2 2 2 2 2 2";
		String result = utils.obtenerValorPorEspacio(valor,1);
		assertNotNull(result);
		assertTrue(!result.equals(""));
	}
		
}
