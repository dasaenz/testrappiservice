package com.test.backend.converter;


import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.test.rest.pojo.InputMatriz;

public class InfoConverterTest {

	InfoConverter infoConverter = new InfoConverter();
	
	@Test
	public void convertirEntradaInfo() throws Exception{
		String salto = "\n";
		String valor= "2"; 
		valor += salto+"4 5";
		valor += salto+"UPDATE 2 2 2 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"UPDATE 1 1 1 23";
		valor += salto+"QUERY 2 2 2 4 4 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"2 4";
		valor += salto+"UPDATE 2 2 2 1";
		valor += salto+"QUERY 1 1 1 1 1 1";
		valor += salto+"QUERY 1 1 1 2 2 2";
		valor += salto+"QUERY 2 2 2 2 2 2";
		InputMatriz inputMatriz = infoConverter.convertirEntradaInfo(valor);
		assertNotNull(inputMatriz);
	}
}
