package com.test.rest.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;

public class MatrizServiceTest {

	MatrizService matrizService = new MatrizService();
	
	@Test
	public void calcularMatriz() {
		String salto = "<br>";
		String valor= "2"; 
		valor += salto+"4 5";
		valor += salto+"UPDATE 2 2 2 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"UPDATE 1 1 1 23";
		valor += salto+"QUERY 2 2 2 4 4 4";
		valor += salto+"QUERY 1 1 1 3 3 3";
		valor += salto+"2 4";
		valor += salto+"UPDATE 2 2 2 1";
		valor += salto+"QUERY 1 1 1 1 1 1";
		valor += salto+"QUERY 1 1 1 2 2 2";
		valor += salto+"QUERY 2 2 2 2 2 2";
		List<Integer> lstResult = matrizService.calcularMatriz(valor, null);
		assertNotNull(lstResult);
		lstResult.forEach(y->System.out.println(y));
	}
}
