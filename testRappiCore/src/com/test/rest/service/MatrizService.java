package com.test.rest.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import com.test.backend.converter.InfoConverter;
import com.test.backend.exception.MatrizException;
import com.test.backend.utils.OperacionesUtils;
import com.test.backend.utils.TipoOperacion;
import com.test.rest.pojo.InfoMatriz;
import com.test.rest.pojo.InputMatriz;
import com.test.rest.pojo.OperacionMatriz;

@Path("/matrizService")
public class MatrizService {
		
	@GET
	@Produces("application/json")
	@Path("/calcularMatriz/{data}")
	public List<Integer> calcularMatriz(
			@PathParam("data") String data, @Context HttpServletRequest request) {
		List<Integer> lstResult = new ArrayList<>();
		InfoConverter infoConverter = new InfoConverter();
		try {
			InputMatriz inputMatriz = infoConverter.convertirEntradaInfo(data);
			inputMatriz.getLstInfo().stream().
			filter(p->inputMatriz.getNumeroCasos()>=1 && inputMatriz.getNumeroCasos()<=50 &&
			 p.getDimension()>=1 && p.getDimension()<=100 &&
			 p.getNumeroOperaciones()>=1 && p.getNumeroOperaciones()<=1000
			).
			forEach(p->{
			  p.getDimension();
			  p.getNumeroOperaciones();
			  inputMatriz.setMiMatriz(new int[p.getDimension()+1][p.getDimension()+1][p.getDimension()+1]);
			  p.getLstOperaciones().stream().filter(c->
			      c.getX1()>=1 && (c.getX2()==0 || c.getX1()<=c.getX2()) && c.getX2()<=p.getDimension() &&
			      c.getY1()>=1 && (c.getY2()==0 || c.getY1()<=c.getY2()) && c.getY2()<=p.getDimension() &&
			      c.getZ1()>=1 && (c.getZ2()==0 || c.getZ1()<=c.getZ2()) && c.getZ2()<=p.getDimension()
			  ).
			  forEach(c->{
				  int xIni=c.getX1()-1;
				  int yIni=c.getY1()-1;
				  int zIni=c.getZ1()-1;
				  int xFin=c.getX2()-1;
				  int yFin=c.getY2()-1;
				  int zFin=c.getZ2()-1;
				  if(c.getTipoOperacion().equals(TipoOperacion.UPDATE)){
					  inputMatriz.getMiMatriz()[xIni][yIni][zIni] = c.getValor(); 
				  }else{
					 int resultado= OperacionesUtils.sum(xIni, yIni, zIni, xFin, yFin, zFin, inputMatriz.getMiMatriz());
					 lstResult.add(resultado);
				  }
				 });
			  });
		}
		catch(NumberFormatException m) {
			lstResult.add(-1);
		}
		catch(Exception m) {
			lstResult.add(-2);
		}
		return lstResult;
	}
}
