package com.test.rest.pojo;

import java.util.List;

public class InputMatriz {

	private int numeroCasos;
	private List<InfoMatriz> lstInfo;
	private int[][][] miMatriz;
	
	public int getNumeroCasos() {
		return numeroCasos;
	}
	public void setNumeroCasos(int numeroCasos) {
		this.numeroCasos = numeroCasos;
	}
	public List<InfoMatriz> getLstInfo() {
		return lstInfo;
	}
	public void setLstInfo(List<InfoMatriz> lstInfo) {
		this.lstInfo = lstInfo;
	}
	public int[][][] getMiMatriz() {
		return miMatriz;
	}
	public void setMiMatriz(int[][][] miMatriz) {
		this.miMatriz = miMatriz;
	}
	
	
}
