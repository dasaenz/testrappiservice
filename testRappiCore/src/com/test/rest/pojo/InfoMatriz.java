package com.test.rest.pojo;

import java.util.List;

public class InfoMatriz {
	
	private int dimension;
	private int numeroOperaciones;
	private List<OperacionMatriz> lstOperaciones;
	
	
	public int getDimension() {
		return dimension;
	}
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}
	public int getNumeroOperaciones() {
		return numeroOperaciones;
	}
	public void setNumeroOperaciones(int numeroOperaciones) {
		this.numeroOperaciones = numeroOperaciones;
	}
	public List<OperacionMatriz> getLstOperaciones() {
		return lstOperaciones;
	}
	public void setLstOperaciones(List<OperacionMatriz> lstOperaciones) {
		this.lstOperaciones = lstOperaciones;
	}
	
	
	
}
