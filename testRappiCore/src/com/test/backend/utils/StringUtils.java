package com.test.backend.utils;

public class StringUtils {

	public static String obtenerValorPorEspacio(String value, int posicion){	
		String valor = "";
		if(value!=null && !value.equals("")){
			String[] data = value.split("\\s+");
			valor = data[posicion]; 
		}
		return valor;
	}
	
	public static String obtenerLinea(String value, int posicion){
		String valor = "";
		if(value!=null && !value.equals("")){
			String[] data = value.split("<br>");
			valor = data[posicion]; 
		}
		return valor;
	}
		
}
