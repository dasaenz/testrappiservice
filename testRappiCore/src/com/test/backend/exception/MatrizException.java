package com.test.backend.exception;

public class MatrizException extends Exception{

	public MatrizException(String mensaje, Exception e) {
		super(mensaje, e);
	}
}
