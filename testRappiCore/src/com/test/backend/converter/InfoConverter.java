package com.test.backend.converter;

import java.util.ArrayList;
import java.util.List;

import com.test.backend.exception.MatrizException;
import com.test.backend.utils.StringUtils;
import com.test.backend.utils.TipoOperacion;
import com.test.rest.pojo.InfoMatriz;
import com.test.rest.pojo.InputMatriz;
import com.test.rest.pojo.OperacionMatriz;

public class InfoConverter {

	/**
	 * Se realiza el mapeo de la entrada a los pojos
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public InputMatriz convertirEntradaInfo(String data) throws Exception{
		InputMatriz input = new InputMatriz();
		try{						
			String numeroCasos = StringUtils.obtenerLinea(data, 0);	
			int numeroCasosInt = Integer.parseInt(numeroCasos);
			input.setNumeroCasos(numeroCasosInt);
			List<InfoMatriz> lstInfoMatriz = new ArrayList<InfoMatriz>();
			
			int indiceLinea=1;
			for(int i =0;i<numeroCasosInt; i++) {
			   InfoMatriz info = new InfoMatriz();
			   List<OperacionMatriz> lstOperaciones = new ArrayList<OperacionMatriz>();
			   String linea = StringUtils.obtenerLinea(data, indiceLinea);	
			   int dimensiones = Integer.valueOf(StringUtils.obtenerValorPorEspacio(linea, 0));
		       int numOperaciones = Integer.valueOf(StringUtils.obtenerValorPorEspacio(linea, 1));
		       
		       info.setDimension(dimensiones);
		       info.setNumeroOperaciones(numOperaciones);
		       		
		       indiceLinea++;
		       for(int z=0;z<numOperaciones;z++){
		    	   OperacionMatriz operacionMatriz = new OperacionMatriz();
		    	   String lineaOpe = StringUtils.obtenerLinea(data, indiceLinea);
		    	   String tipoOperacion = StringUtils.obtenerValorPorEspacio(lineaOpe, 0);
		    	   operacionMatriz.setX1(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 1)));
		    	   operacionMatriz.setY1(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 2)));
		    	   operacionMatriz.setZ1(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 3)));
		    	   if(tipoOperacion.equals(TipoOperacion.QUERY.name())){
		    		   operacionMatriz.setTipoOperacion(TipoOperacion.QUERY);			    	   
		    		   operacionMatriz.setX2(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 4)));
			    	   operacionMatriz.setY2(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 5)));
			    	   operacionMatriz.setZ2(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 6)));
		    	   }else{
		    		   operacionMatriz.setTipoOperacion(TipoOperacion.UPDATE);
		    		   operacionMatriz.setValor(Integer.valueOf(StringUtils.obtenerValorPorEspacio(lineaOpe, 4)));			    	   
		    	   }
		    	  
		    	   indiceLinea++;
		    	   lstOperaciones.add(operacionMatriz);
		       }
		       info.setLstOperaciones(lstOperaciones);
		       lstInfoMatriz.add(info);
			}
			input.setLstInfo(lstInfoMatriz);
			
		}catch(Exception e){
			throw e;
		}
		return input;
	}
}
